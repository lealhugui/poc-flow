from django.urls import path
from .views import get_fluxo_view, atualiza_status_view, get_fluxo_list

urlpatterns = [
    path('<int:id_fluxo>', get_fluxo_view, name='fluxo-form'),
    path('<int:id_fluxo>/atualizar', atualiza_status_view, name='atualiza-fluxo'),
    path('', get_fluxo_list, name='fluxo-list')
]