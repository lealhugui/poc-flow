from django.shortcuts import render, redirect, HttpResponse
from django.views.generic.list import ListView
from django import forms
from dynaforms.views import generate_form_instance, get_aprovers, get_possible_status
from django.contrib.auth.models import User, Group
from dynaforms.models import Status, StatusPefil
from .models import Fluxo


def get_status_form(status_obj):
    choices = []
    for s in get_possible_status(status_obj):
        choices.append((s.id, s.label))
    
    choice_field = forms.ChoiceField(choices=choices)
    return type('StatusForm', (forms.Form,), {'status': choice_field})

# Create your views here.
def get_fluxo_view(request, id_fluxo):
    fluxo = Fluxo.objects.get(id=id_fluxo)
    frm =  generate_form_instance(fluxo.status.id, [c.id for c in fluxo.categorias.all()])

    ctx = {
        'dynaform': frm,
        'title': f'Fluxo "{id_fluxo}"',
        'fluxo': fluxo,
        'status_atual': fluxo.status.label,
        'status_form': get_status_form(fluxo.status)
    }
    return render(request, 'forms/form_fluxo.html', ctx)

def atualiza_status_view(request, id_fluxo):
    if request.method != 'POST':
        raise Exception('Não é permitido')
    
    f = Fluxo.objects.get(id=id_fluxo)
    f.status = Status.objects.get(id=request.POST['status'])
    f.save()

    return redirect('fluxo-list')

def get_fluxo_list(request):
    usr = None
    qs = Fluxo.objects.all()
    usuarios = User.objects.all()
    if request.method == 'POST':
        if len(request.POST['usuario']) == 0:
            raise ValueError('Erro ao informar usuario')
        usr = User.objects.get(id=request.POST['usuario'][0])
        
        for u in usuarios:
            u.selected = u.id == usr.id
        
        status = StatusPefil.objects.filter(
            perfil_id__in = [g.id for g in usr.groups.all()]
        )

        qs = qs.filter(status_id__in=[s.id for s in status])



    ctx = {
        'object_list': qs,
        'usuarios': usuarios
    }

    return render(request, 'list/list_fluxo.html', ctx)

class FluxoListView(ListView):
    template_name = 'list/list_fluxo.html'
    model = Fluxo
    paginate_by = 30


    