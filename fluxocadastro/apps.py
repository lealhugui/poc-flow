from django.apps import AppConfig


class FluxocadastroConfig(AppConfig):
    name = 'fluxocadastro'
