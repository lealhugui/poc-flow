from django.db import models

# Create your models here.
class Fluxo(models.Model):

    class Meta:
        ordering = ['status', '-id']

    dados = models.TextField()
    status = models.ForeignKey('dynaforms.Status', on_delete=models.PROTECT)
    categorias = models.ManyToManyField('dynaforms.Categoria')