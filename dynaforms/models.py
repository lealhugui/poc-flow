from enum import Enum
from django.db import models
from django.contrib.auth.models import Group

# Create your models here.

class Workflow(models.Model):
    label = models.CharField(max_length=50)

class Status(models.Model):

    workflow = models.ForeignKey('Workflow', on_delete=models.CASCADE)

    label = models.CharField(max_length=50)

class StatusPefil(models.Model):

    status = models.ForeignKey('Status', on_delete=models.CASCADE)
    perfil = models.ForeignKey('auth.Group', on_delete=models.CASCADE)

class Transicao(models.Model):
    status_origem = models.ForeignKey('Status', related_name='para_transicao', on_delete=models.CASCADE, null=True)
    status_destino = models.ForeignKey('Status', related_name='de_transicao', on_delete=models.CASCADE, null=True)

class TipoCampo(Enum):
    NUMERICO='NUMERICO'
    TEXTO='TEXTO'
    DATA='DATA'
    SELECT='SELECT'
    CHECKBOX='CHECKBOX'

class Campo(models.Model):

    TIPOS_CAMPO = (
        (TipoCampo.NUMERICO, 'NUMERICO'),
        (TipoCampo.TEXTO, 'TEXTO'),
        (TipoCampo.DATA, 'DATA'),
        (TipoCampo.SELECT, 'SELECT'),
        (TipoCampo.CHECKBOX, 'CHECKBOX')
    )

    label = models.CharField(max_length=50)
    tipo = models.CharField(max_length=10, choices=TIPOS_CAMPO, default=TipoCampo.TEXTO)
    tamanho = models.IntegerField()
    precisao = models.IntegerField(null=True)
    sugestao = models.CharField(max_length=200, null=True)
    default = models.CharField(max_length=200, null=True)
    prefixo = models.CharField(max_length=200, null=True)


class Categoria(models.Model):
    pai = models.ForeignKey('self', on_delete=models.CASCADE, null=True)
    label = models.CharField(max_length=100)
    nivel = models.IntegerField()

    campos = models.ManyToManyField('Campo', through='CampoCategoria')


class CampoCategoria(models.Model):
    categoria = models.ForeignKey('Categoria', on_delete=models.CASCADE)
    campo = models.ForeignKey('Campo', on_delete=models.CASCADE)
    dominio = models.CharField(max_length=50, null=True)
    sufixo = models.CharField(max_length=50, null=True)


class Tela(models.Model):

    class Meta:
        unique_together = [['ordenacao', 'status']]

    status = models.ForeignKey('Status', on_delete=models.CASCADE)
    campo_categoria = models.ForeignKey('CampoCategoria', on_delete=models.CASCADE)
    ordenacao = models.IntegerField()
    obrigatorio = models.BooleanField(default=False)
    editavel = models.BooleanField(default=True)

class PermissaoStatus(models.Model):

    status = models.ForeignKey('Status', on_delete=models.CASCADE)
    permissao  = models.ForeignKey('auth.Permission', on_delete=models.CASCADE)

class PerfilCategoria(models.Model):

    perfil = models.ForeignKey('auth.Group', on_delete=models.CASCADE)
    categoria = models.ForeignKey('Categoria', on_delete=models.CASCADE, null=True)
