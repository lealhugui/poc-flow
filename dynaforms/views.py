import datetime
from django import forms
from django.contrib.auth import models as auth_models
from .models import *

def to_form_field(tela_record):
    field = tela_record.campo_categoria.campo
    f = None
    opts = {        
        'required': tela_record.obrigatorio,
        'disabled': not tela_record.editavel
    }
    if field.tipo == TipoCampo.TEXTO.value:
        return forms.CharField(max_length=field.tamanho or 50, **opts)
    elif field.tipo == TipoCampo.NUMERICO.value:
        return forms.DecimalField(widget=forms.NumberInput(), **opts)
    elif field.tipo == TipoCampo.DATA.value:
        return forms.DateField(
            initial=datetime.date.today,
            input_formats=['%d/%m/%Y'],
            widget=forms.SelectDateWidget(),
            **opts
        )
    elif field.tipo == TipoCampo.SELECT.value:
        if not field.sugestao or field.sugestao == '':
            raise ValueError('Campo select sem sujestao definida')
        
        opts['choices'] = [(s.strip(), s.strip()) for s in field.sugestao.split(';')]
        return forms.ChoiceField(**opts)
    elif field.tipo == TipoCampo.CHECKBOX.value:
        default = True if field.default == 'True' else False
        opts['initial'] = default

        # deve ser required False, pois de outra forma o campo
        # nao aceitará o valor "False" na checkbox
        opts['required'] = False
        return forms.BooleanField(**opts)
    else:
        raise ValueError('Tipo desconhecido')

    return f

# Create your views here.
def generate_form_instance(id_status, ids_categoria):
    for c in ids_categoria:
        if not isinstance(c, int):
            raise ValueError(f'Categoria "{c}" inválida')

    status = Status.objects.get(id=id_status)

    if status is None:
        raise ValueError(f'Status "{id_status}" não encontrado')

    filters = {
        'status': status,
        'campo_categoria__categoria_id__in': ids_categoria
    }

    telas = Tela.objects.filter(**filters).order_by('ordenacao')
    form_name = "DynaForm"

    fields = {}
    for t in telas:
        fields[t.campo_categoria.campo.label] = to_form_field(t)

    t = type(form_name, (forms.Form,), fields)

    return t

def get_possible_status(status):
    transicoes = Transicao.objects.filter(status_origem=status)
    return [] if len(transicoes) == 0 else [t.status_destino for t in transicoes]

def get_aprovers(status):
    apr = Aprovador.objects.filter(status=status)

    if len(apr) == 0:
        return []

    usrs = []
    for a in apr:
        u = auth_models.User.objects.filter(groups__id__in=a.perfil_id)
        usrs += u

    return usrs
