from django.apps import AppConfig


class DynaformsConfig(AppConfig):
    name = 'dynaforms'
